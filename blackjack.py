import random, os
from art import logo
def blackjack_game():
    clear = lambda: os.system('cls' if os.name == 'nt' else 'clear')
    clear()
    print(f"{logo} \n \n")
    cards = {
        "A♣" : 11,
        "2♣" : 2,
        "3♣" : 3,
        "4♣" : 4,
        "5♣" : 5,
        "6♣" : 6,
        "7♣" : 7,
        "8♣" : 8,
        "9♣" : 9,
        "10♣" : 10,
        "J♣" : 10,
        "Q♣" : 10,
        "K♣" : 10,
        "A♦" : 11,
        "2♦" : 2,
        "3♦" : 3,
        "4♦" : 4,
        "5♦" : 5,
        "6♦" : 6,
        "7♦" : 7,
        "8♦" : 8,
        "9♦" : 9,
        "10♦" : 10,
        "J♦" : 10,
        "Q♦" : 10,
        "K♦" : 10,
        "A♥" : 11,
        "2♥" : 2,
        "3♥" : 3,
        "4♥" : 4,
        "5♥" : 5,
        "6♥" : 6,
        "7♥" : 7,
        "8♥" : 8,
        "9♥" : 9,
        "10♥" : 10,
        "J♥" : 10,
        "Q♥" : 10,
        "K♥" : 10,
        "A♠" : 11,
        "2♠" : 2,
        "3♠" : 3,
        "4♠" : 4,
        "5♠" : 5,
        "6♠" : 6,
        "7♠" : 7,
        "8♠" : 8,
        "9♠" : 9,
        "10♠" : 10,
        "J♠" : 10,
        "Q♠" : 10,
        "K♠" : 10,
    }

    deck = list(cards.keys())
    player_cards = []
    computer_cards = []

    def deal_card(deck,user):
        random_card = random.choice(deck)
        deck.pop(deck.index(random_card))
        if user == "player":
            player_cards.append(random_card)
        elif user == "computer":
            computer_cards.append(random_card)

    sum_of_cards_player = 0
    sum_of_cards_computer = 0
    game_over = False

    def get_sum_player(sum_of_cards_player=0):
        count_a = 0
        for item in player_cards:
            if item == "A♣" or item == "A♦" or item == "A♥" or item == "A♠":
                count_a += 1
            sum_of_cards_player += cards[item]

        if sum_of_cards_player > 21: 
            #print("A is there")
            if count_a != 0:
                sum_of_cards_player -= (10 * count_a)

        return sum_of_cards_player 

    def get_sum_computer(sum_of_cards_computer=0):
        count_a = 0
        for item in computer_cards:
            if item == "A♣" or item == "A♦" or item == "A♥" or item == "A♠":
                count_a += 1
            sum_of_cards_computer += cards[item]
        
        if sum_of_cards_computer > 21: 
            #print("A is there")
            if count_a != 0:
                sum_of_cards_computer -= (10 * count_a)

        return sum_of_cards_computer
        
    for _ in range(2):
        deal_card(deck,"player")
        deal_card(deck,"computer")

    #player_cards = ["A♣","5♣"]
    #computer_cards = ["A♣","10♣"]

    print(f"Your cards = {player_cards}")
    print(f"Computer's cards = ['{computer_cards[0]}' , X ]")

    def check_blackjack(game_over):
        if get_sum_computer(sum_of_cards_computer) == 21:
            print("Computer Blackjack!! You lose :(")
            game_over = True
        elif get_sum_player(sum_of_cards_player) == 21:
            print("You have Blackjack!! You win :(")
            game_over = True
        else:
            game_over = False
        return game_over

    game_over = check_blackjack(game_over)
    hit_again = True
    while not game_over:
        if get_sum_player(sum_of_cards_player) <= 21:
            if get_sum_player(sum_of_cards_player) == 21:
                hit_again = False
                again = "N"

            if hit_again and get_sum_player(sum_of_cards_player) != 21:
                again = input("Hit? (Y/N):- ").upper()

            if again == "Y":
                deal_card(deck,"player")
                print(f"Your cards = {player_cards}")

            else:
                hit_again = False
                print(f"Your cards = {player_cards}")
                print(f"Computer's cards = {computer_cards}")

                if get_sum_computer(sum_of_cards_computer) > 21:
                    game_over = True
                    print("Computer went over. You won! :)")
                
                elif get_sum_computer(sum_of_cards_computer)<17:
                    deal_card(deck,"computer")
                    print("Computer Hits!")

                elif get_sum_computer(sum_of_cards_computer) > get_sum_player(sum_of_cards_player):
                    #print(f"Your cards = {player_cards}")
                    #print(f"Computer's cards = {computer_cards}")
                    game_over = True
                    print("You lost! :(")
                
                elif get_sum_computer(sum_of_cards_computer) == get_sum_player(sum_of_cards_player):
                    game_over = True
                    print("It's a draw")

                else:
                    game_over = True
                    print("You won! :)")
        
        elif get_sum_player(sum_of_cards_player) > 21:
            #print(f"Your cards = {player_cards}")
            game_over = True
            print(f"Computer's cards = {computer_cards}")
            print("You went over. You lost! :(")
    
    if input("Play another game? (Y/N):- ").upper() == "Y":
        blackjack_game()

blackjack_game()




    



    
